#MIT License
#Author: Justin Smith

import tkinter as tk   
import math

from tkinter import ttk


def sats_usd():
    sats = float(ent_sats3.get())
    btcusd = float(ent_btcusd3.get()) 
    result = (sats * btcusd) / 100000000
    lbl_result03["text"] = f"{result} USD"
    lbl_result03.after(1000, sats_usd) #auto updates text every 1000 ms after you first click the arrow

    
def usd_sats():
    usd = float(ent_usd.get()) 
    btcusd = float(ent_btcusd.get())
    result = (usd * 100000000) / btcusd
    lbl_result0["text"] = f"{result} SATs"
    lbl_result0.after(1000, usd_sats) #auto updates text every 1000 ms after you first click the arrow
    
  
  
root = tk.Tk()
root.title("SAT/USD Conversions")
tabControl = ttk.Notebook(root)

#set center screen window with following coordination
win_left_pos = (root.winfo_screenwidth() - 500) / 2
win_right_pos = (root.winfo_screenheight() - 300) / 2
root.geometry( "%dx%d+%d+%d" % (500, 300, win_left_pos, win_right_pos)) #Set window width, height, position



tab1 = ttk.Frame(tabControl)
tab2 = ttk.Frame(tabControl)

tabControl.add(tab1, text ='Convert USD to SAT')
tabControl.add(tab2, text ='Convert SAT to USD')
tabControl.pack(expand = 1, fill ="both")
  

ttk.Label(tab1, 
          text ="USD to SAT"
          ).grid(column = 0, 
            row = 0,
            padx = 30,
            pady = 30
            )  


#Tab1: usd to sats
frm_entry0 = tk.Frame(tab1)
ent_usd = tk.Entry(master=frm_entry0, width=10)
lbl_usd = tk.Label(master=frm_entry0, text="USD Amount")

ent_btcusd = tk.Entry(master=frm_entry0, width=10)
lbl_btcusd = tk.Label(master=frm_entry0, text="BTC Price")

# Layout the Entry and Label in frm_entry0
# using the .grid() geometry manager
ent_usd.grid(row=0, column=0, sticky="e")
lbl_usd.grid(row=0, column=1, sticky="w")

ent_btcusd.grid(row=1, column=0, sticky="e")
lbl_btcusd.grid(row=1, column=1, sticky="w")

# Create the conversion Button and result display Label
btn_convert = tk.Button(
    tab1,
    text="\N{RIGHTWARDS BLACK ARROW}",
    command=usd_sats
)

lbl_result0 = tk.Label(tab1, text="SATs")

# Set-up the layout using the .grid() geometry manager
frm_entry0.grid(row=0, column=0, padx=10)
btn_convert.grid(row=0, column=1, pady=10)
lbl_result0.grid(row=0, column=2, padx=10)

ttk.Button(tab1, 
            text="Exit", 
            command=root.quit
            ).grid(column = 2,
            row = 2, 
            padx = 30,
            pady = 30
            )


#Tab2: sats to usd
frm_entry = tk.Frame(tab2)
ent_sats3 = tk.Entry(master=frm_entry, width=10)
lbl_sats3 = tk.Label(master=frm_entry, text="SATs")

ent_btcusd3 = tk.Entry(master=frm_entry, width=10)
lbl_btcusd3 = tk.Label(master=frm_entry, text="BTC Price")

# Layout
ent_sats3.grid(row=0, column=0, sticky="e")
lbl_sats3.grid(row=0, column=1, sticky="w")

ent_btcusd3.grid(row=1, column=0, sticky="e")
lbl_btcusd3.grid(row=1, column=1, sticky="w")

# Convert button
btn_convert3 = tk.Button(
    master=tab2,
    text="\N{RIGHTWARDS BLACK ARROW}",
    command=sats_usd
)

lbl_result03 = tk.Label(master=tab2, text="USD")

# Layout
frm_entry.grid(row=0, column=0, padx=10)
btn_convert3.grid(row=0, column=1, pady=10)
lbl_result03.grid(row=0, column=2, padx=10)

btn3 = ttk.Button(tab2, 
            text="Exit", 
            command=root.quit
            ).grid(column = 2,
            row = 2, 
            padx = 30,
            pady = 30
            )
            

root.mainloop() 
