<html>

<body>
  <h1>
    Transacting Real Property Implementation
  </h1>
  <p>
    In <i>Transacting Real Estate Title using Blockchain Technology</i> I wrote that the system proposed for managing
    real estate title may be implemented as peer-to-peer software. What would such a software look like? We would need
    a flexible design
    that is aligned with the Unix Philosophy: highly specialized modules which are interoperable. This way we can add
    or remove capabilities easily, without causing significant disruptions to the system.
  </p>
  <p>If we follow this design philosophy when we architect our system, we'll have a system which is easy to maintain,
    easy to test and debug, and easy to build on. This would give us the stability and reliability performance that you
    would expect from a
    computer system that you are using to manage trillions of dollars worth of assets. Fortunately there are already
    similar systems which have been proven to handle large amounts of value, such as Bitcoin and Monero, from which we
    can draw architectural
    inspiration.
  </p>
  <p>Here's a high level look at how our real property system might be architected:</p>
  </p>
  <p>
      <center><img src="system.svg" width=850px></br></br>
    Fig. 1. System Overview</center>
  </p>
  <p>
    We're trying to translate customs and procedures into computer software, so we need to involve a number of
    different tools and techniques. The task might seem simple: keep record of who owns what real property, make sure
    that record can only be changed under the appropriate circumstances, and make that record publicly available. But
    when we examine the problem further we find that we need to consider identity management, the mutability of
    records, and the availability of records. Until now, this is done almost entirely by humans.
  </p>
  <p>
    When designing the architecture of a software system, we must start wtih top level requirements. We might be
    missing a number of requirements too, but we're not yet aware of it. Unless all of the requirements are fulfilled
    the system will be useless, as
    it won't match the existing capabilities of most civil and common law property systems. If this standardized
    electronic legal system doesn't offer at least the same capabilities as the existing paper system, why would anyone
    go to the trouble of trying
    something new?

    <ul>
      <li>
        We must be able to accurately copy the existing real property ledger, which in most regions is a set of paper
        documents, and maybe a file server, kept in an office in the same region as the real property. We want to make
        sure this information does not
        change when we bootstrap this software system.
      </li>
      <li>
        This new electronic record cannot disappear or be removed under any circumstances.
      </li>
      <li>
        When it is time to update the record, there cannot be any technical delay or limiting factor. This does not
        mean that there is no cost (or that you can always expect a low cost) to do this!
      </li>
      <li>
        The update of the record may only be initialized by the current owner(s) of the property. This means that if in
        your jurisdiction a land registry must sign off on all transactions, then the land registry is actually a
        co-owner of the property! In some
        regions, you might have multiple government offices which may be able to seize real property directly, without
        the consent of the land registry office- if that's the case, then these group should also be considered
        co-owners of the property.
      </li>
      <li>
        There should be a way to manage liens, which is important for property which exists in a legal system where
        encumbering real property title is common.
      </li>
      <li>
        We need a proven way to make sure that the people involved in any changes to the record are who they say they
        are in order to prevent fraud.
      </li>
      <li>
        We should be able to do useful things with the software, such as manage and track rental income and expenses.
      </li>
    </ul>
  </p>
  <h3>Daemon</h3>
  <p>
    The daemon is the centerpiece of our system, acting as a hub which coordinates communication between important
    components, such as an LMDB database, a bitcoin wallet containing information about your bitcoin, a property wallet
    containing information about
    your real property, a graphical user interface (GUI), and the outside network.
  </p>
  <p>
    The daemon is an application which runs locally on your laptop or desktop computer, and synchronizes your local
    property wallet to the current state of the global property P2P ledger. When you update an important record, such
    as a title deed or title
    insurance document, the daemon would help you record this update in a way that is pushed to the network and becomes
    permanent.
  </p>

  <h3>API</h3>
  <p>
    The daemon exposes an Application Programming Interface, allowing external applications to read and write to
    certain parts of your local database. This is important for a number of activities, such as:
    <ul>
      <li>
        Counterparties digitally signing contracts, e.g. rental agreements.
      </li>
      <li>
        Delegating activities, e.g. asking your property manager to enter the rents that were received last month.
      </li>
      <li>
        Viewing your property wallet data from a lightweight client, such as a smartphone app.
      </li>
      <li>
        Allowing companies, such as banks and insurance providers, to access some of your data.
      </li>
    </ul>
  </p>

  <p>Most users are very comfortable with using their smartphone for simple actions. While there isn't much legal case
    history yet, signatures created on mobile devices with multifactor authentication (something the user knows +
    something the user has) should be considered just as valid as traditional "chicken scratch" with ink and paper.
    Unlike signatures created by hand, digital signatures are nearly impossible to forge.
  </p>
  <p>
      <center><img src="sig-device.svg" width=850px><br><br>
    Fig. 2. Tools for an investor in rental properties.</center>
  </p>
  <p>
    For security and privacy, these mobile apps can connect directly to the user's workstation. Or, these apps can
    access and decrypt the last known state of the workstation from the cloud. This is very much like Monero, where
    data are downloaded from a persistent P2P network and decrypted on the user's device.
  </p>

  <!-- <p>
  Zero Knowledge succinct non-interactive arguments of knowledge let you prove that something is true, without you needing to reveal that something. For example, you might want to prove to a lender that you have sufficient funds available to pay a mortgage in full, but you don't necessarily want them to know how much money you have.
</p> -->

  <h3>Document Timestamping</h3>
  <p>
    When an important document is created or revised, a hash of that document needs to be created and published. This
    acts as a receipt, or proof, that the document has been altered. This, along with permanent publication of the
    documents on a distributed
    network and identity verification makes it difficult to commit fraud.
  </p>
  <p>
    How is the document timestamped? The SHA256 hash is included as a dummy output in a bitcoin transaction. The best
    tool for the job is probably the <a href="https://opentimestamps.org/">Open Timestamps project</a>, which is also
    using the Bitcoin
    blockchain.
  </p>
  <h3>Backups</h3>
  <p>
    Backing up important data is a common practice, and that backup becomes increasingly more important as that data
    becomes more valuable. A title deed to a USD 30'000'000 beach front property with your name on it is probably more
    important to protect than
    the gif you found on Telegram this morning.
  </p>
  <p>
    Since we are dealing with such important documents, we want to ensure they will always be available anytime you
    have an internet connection: on demand, and under any conditions. This means we don't want to rely on a data
    custodian who we would have to
    pay to backup our data for us. Instead, we'll want to use something like the free and open source Interplanetary
    File System (IPFS). We can tweak this protocol and create our own application-specific IPFS network, which should
    provide just as much
    security as the global IPFS network but with dramatically improved speed. The downside to this approach is our
    software application will require more local disk storage.
  </p>
  <h3>Identity</h3>
  <p>
    What can we do to prevent someone from installing this software, then claiming they own property that they really
    don't? We need to establish a system where participants are able to properly identify themselves. We can do this by
    using a notary public.
    A notary is a trusted third party, often certified by some legally recognized institution, who can validate
    signatures. They check your identification document(s), watches you and your counterparty sign a document, then
    place their signature or
    stamp of approval on the same document. This process ensures that neither party can later claim that they did not
    sign the document.
  </p>
  <p>
    This signing process doesn't have to be done on paper, it can be done digitally. And with the internet, we don't
    need to be geographically co-located with our counter-party, <i>we only need to be co-located with our notary</i>.
    If our notary can
    ensure our business partner's notary is legitimate, then we can trust our business partner's signature. This is
    simply a <a href="https://en.wikipedia.org/wiki/Web_of_trust">web of trust (WOT)</a>, which has been in use
    successfully for many decades.
  </p>
  <p>
    To make this process easier, we can use a simple mobile application for an iOS or Android smartphone that allows us
    to sign contracts. We would go to the notary with our app on our phone, show a government ID, and the notary can
    watch us sign a digital
    contract. This app can connect to our daemon over the system's API, making it very easy to quickly and securely
    sign rental contracts, purchase agreements, etc.
  </p>
  <h3>P2P Network</h3>
  <p>
    Another interesting benefit from networking these instances is that you can expose some data to the public network
    which is useful at a certain time. This could make it really easy to buy and sell property- click a box that says
    "for sale" and important
    data about your property would be made publicly available. Like blockchain explorers, third parties would likely
    create property explorers to curate and display the data they collect from the network.
  </p>
  <p>
    You might choose expose some data to a bank or insurer, which may then be able to make you a better offer on a
    mortgage or insurance policy. With bitcoin payment channels, it is possible to create incentives which would allow
    a market for this data to
    form.
  </p>
  <h3>Data</h3>
  <p>
    Common corporate real estate solutions such as <a href="https://www.sap.com/products/real-estate-management.html">SAP
      (warning: javascript)</a> and <a href="http://www.costar.com/customers/owners-investors">Costar</a> are the
    primitive ancestors
    of our proposed system. These are simple permissioned databases which lack interoperability and modern capabilities
    - and organizations pay dearly to license them. The quality of the data entered into these systems is high, but the
    value of this
    data doesn't have a price tag on it- so it's impossible to know just how valuable your data is, and whether or not
    you are making best use of it.
  </p>
  <p>
    As an alternative to SAP, Costar, and similar proprietary solutions with traditional licensing models, we can (and
    should) create a free, open source (FOSS) real property software which offers similar database features. This is a
    traditional "FOSS alternative"
    approach, just like Libre Office to Microsoft Office, GIMP to Adobe Photoshop, etc. These projects rely on
    donations to keep their development going. The price difference alone is sometimes enough to get users to ditch a
    proprietary solution. But
    we can offer much more powerful features than these database solutions, and we wouldn't need to run this project
    off of donations.
  </p>
  <p>
    Financial incentives can be very effective in shaping behavior. We can use bitcoin, a programmable money, not only
    as an integrated means of conveyance for smart contracts in real property transactions (a small fee could be added
    to transactions to support
    this project), but also to encourage creation and sharing of high quality data.
  </p>
  <p>
    What would this data sharing value-transfer landscape look like? For some organizations, data sharing would take a
    business-to-business focus, where you're looking to share some of your data with other organizations that can help
    you in some way. Maybe
    a consultancy is willing to pay you monthly for your rental income data for an industry performance report they are
    planning to publish. Or you could pay a competitor directly to see how well one of their properties is performing.
  </p>

  <p>
    This would be a simple process: you'd select which datasets you are willing to share (if any), and the price that
    you're willing to accept in order to allow strangers to see that data. The entire process is automated from there-
    you wouldn't have to do
    anything but open up your bitcoin wallet and watch the money flow in. You could stop sharing this data at any time.
    How would this be possible? With bitcoin payment channels.
  </p>
  <p>Bitcoin payment channels behave a little bit like a debit card: you pay something to open the account, and you pay
    to close it. The bank may hide this "account opening and closing" cost from depositors, but it's still there. The
    difference with bitcoin
    is that if anything goes wrong while that payment channel is open, you can close it and you are guaranteed to get
    your remaining funds back. That's *not* what happens at a bank- if there's ever a problem with your credit or debit
    card activities,
    the bank can just take any money you have there to solve the problem. Here's an illustration:
  </p>
  <p>
      <center><img src="paychannel.svg" width=850px><br><br>

    Fig. 3. Payment Channels</center>
  </p>
  <p>
    Transfers happening within the payment channel have a marginal cost near zero. This incentive arrangement means
    that data buyers will probably highly evolved specialists, such as consulting firms or tax authorities(!), rather
    than the curious neighbor
    down the street.
  </p>
  <p>
    There is an important caveat: because anyone can use this software, there is a risk of people trying to pollute the
    system with low or poor quality data for reasons which we might not fully understand. We need to create incentives
    which discourage bad
    actors, and encourage and support good actors. We can't guarantee we can keep bad actors out completely, but we can
    make sure that bad actors must pay, and pay dearly, to behave badly. The best way to do this is to require that all
    digital user
    signatures are notarized, and all data is signed before it can be made available for sale. Legitimate users would
    do this anyway, as it is required for signing any smart contract, and illegitimate users won't go to the trouble.
  </p>
</body>

</html>
